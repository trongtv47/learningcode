import 'react-native-gesture-handler';
import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from './src/features/home/screens/home.screen';
import DetailScreen from './src/screens/detail.screen';
import {Provider} from 'react-redux';
import store from './src/store';
import AddScreen from './src/features/home/screens/add.screen';

const Stack = createStackNavigator();
export default function () {
  return (
    <Provider store={store()}>
      <NavigationContainer>
        <Stack.Navigator headerMode="none">
          <Stack.Screen name="home" component={HomeScreen} />
          <Stack.Screen name="detail" component={DetailScreen} />
          <Stack.Screen name="add" component={AddScreen} />
        </Stack.Navigator>
      </NavigationContainer>
    </Provider>
  );
}
