import * as types from './actionTypes';

async function fetchData() {
  var requestOptions = {
    method: 'GET',
    redirect: 'follow',
  };

  return fetch(
    'https://gorest.co.in/public-api/products?page=1',
    requestOptions,
  )
    .then((response) => response.json())
    .then((result) => {
      return result;
    })
    .catch((error) => console.log('error', error));
}

export const getItems = () => async (dispatch) => {

  const data = await fetchData();
  const listData = data.data.map(e => {
    return {
      ...e,
      counterFavorite: 0,
    }
  });
  
  dispatch({
    type: types.GET_ITEMS,
    data: listData,
  });
};

export const countState = (item, data) => async(dispatch) => {
  const newData = data.map(e => {
    if (e.id === item.id) {
      return {
        ...e,
        counterFavorite: e.counterFavorite + 1,
      };
    }
    return e;
  });

  dispatch({
    type: types.COUNT_STATE,
    data: newData,
  });
};

export const addCat = (newCat) => {
  return ({
    type: types.ADD_CAT,
    data: newCat,
  });
};

export const dellCat = (delCat) => {
  return ({
    type: types.DEL_CAT,
    data: delCat,
  });
};
