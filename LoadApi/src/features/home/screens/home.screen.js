import React, { Component, useEffect, useState } from 'react';
import {
  View,
  Text,
  Image,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  FlatList,
  Alert,
} from 'react-native';

import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/FontAwesome';
import Card from '../../../screens/card';
import {connect} from 'react-redux';
import {getItems, countState, dellCat} from '../actions';
import {SwipeListView} from 'react-native-swipe-list-view';


const ItemDto = {
  id: PropTypes.number,
  name: PropTypes.string,
  description: PropTypes.number,
  image: PropTypes.string,
  isFavorite: PropTypes.bool,
  counterFavorite: PropTypes.number,
};


export function HomeScreen({navigation, data, getItems, countState, dellCat}) {
  // const [dataShow, setDataShow] = useState([]);

  const [end, setEnd] = useState(4);
  const [dataShow, setDataShow] = useState([...data.slice(0, end)]);
  


  const [dataShow, setDataShow] = useState([...data.slice(0, end)])
  useEffect(() => {
    getItems();
  }, []);

  useEffect(() => {
    if (dataShow.length < data.length) {
      console.log('datashow', data);
      setDataShow(data.slice(0, end));
    }
  }, [end, data]);

  const loadImg = () => {
    console.log('end:', end);
    setEnd(end + 4);
  };

  const closeRow = (rowMap, rowKey) => {
    if (rowMap[rowKey]) {
      rowMap[rowKey].closeRow();
    }
  };




  const renderItem = ({ item, index }) => {
    return (
      <View>
        <Card>
          <TouchableOpacity
            onPress={() =>
              navigation.navigate('detail', { data: item, data2: index })

            }>
            <ImageBackground
              resizeMode="cover"

              source={{uri: `${item.image}?random=${item.id}`}}
              style={styles.image}>
              <View style={{alignItems: 'flex-end'}}>
                {/* <TouchableOpacity onPress={() => dellCat(item.id)}>
                  <Icon
                    name="trash"
                    style={{fontSize: 25, color: 'red'}}></Icon>
                </TouchableOpacity> */}

                <TouchableOpacity
                  style={{flexDirection: 'row', paddingHorizontal: 16}}
                  onPress={() => countState(item, data)}>
                  <Icon
                    name="star-o"
                    style={{ fontSize: 25, color: 'white' }}></Icon>
                  <Text style={{ marginLeft: 8, color: 'white', fontSize: 20 }}>
                    {item.counterFavorite}
                  </Text>
                </TouchableOpacity>


              </View>
            </ImageBackground>
          </TouchableOpacity>

          <View>
            <View style={styles.flexRow}>
              <Text style={styles.textName}>
                {item.name} - {item.id}
              </Text>
              <Text style={styles.textName}>{item.name} - {item.id}</Text>

            </View>
            <View style={styles.flexRow}>
              <Text style={styles.textDescription} numberOfLines={1}>
                {item.description}
              </Text>
            </View>
          </View>
        </Card>
      </View>
    );
  };

  return (
    <View style={styles.container}>

      {/* <FlatList
        // data={dataShow}
        data={data}
        onEndReachedThreshold={0.5}
        onEndReached={() => {
          // setDataShow(data.slice(dataShow.length, dataShow.length+4))
          loadImg();
        }}
        renderItem={renderItem}
        keyExtractor={(item, index) => index + ''}
      /> */}

      <SwipeListView
        data={data}

        onEndReachedThreshold={0.5}
        onEndReached={() => {
          // setDataShow(data.slice(dataShow.length, dataShow.length+4))
          loadImg();
        }}

        renderItem={renderItem}
        renderHiddenItem={({item}) => (
          <View style={styles.rowBack}>
            <TouchableOpacity
              style={styles.trashBtn}
              // onPress={() => dellCat(item.id)}
              onPress={() => {
                Alert.alert(
                  'Alert',
                  'Are you sure you want to delete?',
                  [
                    {text: 'No', onPress: () => console.log('Cancel'), style:'cancel', },
                    {text: 'Yes', onPress: () =>{
                      dellCat(item.id)
                    }},
                  ],

                  {cancelable: true}
                )
              }}
              
              >
              <Icon name="trash" style={{fontSize: 25, color: 'red'}}></Icon>
              
            </TouchableOpacity>
            
          </View>
        )}
        rightOpenValue={-75}
        keyExtractor={(item, index) => index + ''}
        // keyExtractor={(item, index) => ""+index}

      />

      <View style={styles.floatButton}>
        <TouchableOpacity
          style={styles.button}

          onPress={() => navigation.navigate('addCat')}>
          <Text style={styles.textAdd}>+</Text>

          onPress={() => {
            navigation.navigate('add', {})
          }}

        </TouchableOpacity>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({

  trashBtn: {
    flexDirection: 'row-reverse',
    paddingHorizontal: 25,
    paddingTop: 16,
  },


  image: {
    flex: 1,
    height: 150,
    paddingTop: 16,
    justifyContent: 'space-between',
    marginBottom: 15,
  },

  flexRow: {
    flexDirection: 'row',
    fontWeight: 'bold',
    fontSize: 20,
    color: 'white',
    paddingHorizontal: 10,
  },

  iconsEvilI: {
    flexDirection: 'row',
    fontWeight: 'bold',
    fontSize: 25,
    color: 'white',
    marginLeft: 9,
    color: 'white',
  },

  textName: {
    flexDirection: 'row',
    fontWeight: 'bold',
    fontSize: 16,
    color: '#315fdb',
    marginLeft: 9,
  },

  textDescription: {
    flexDirection: 'row',
    fontSize: 12,
    color: '#4c4c4c',
    marginLeft: 9,
    paddingBottom: 16,
  },

  container: {
    flex: 1,
  },


  activity: {
    flex: 1,
    justifyContent: 'center',
  },
  horizontal: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    padding: 10,
  },

  floatButton: {
    position: 'absolute',
    bottom: 32,
    right: 32,
  },

  button: {
    width: 60,
    height: 60,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#2196F3',
  },

  textAdd: {
    justifyContent: 'center',
    alignItems: 'center',
    fontWeight: 'bold',
    fontSize: 32,
    color: 'white',
  },
});

const mapStateToProps = (state) => {
  return {
    data: state.homeState.data,
  };
};

const mapDispatchToProps = {
  getItems,
  countState,
  dellCat,
};

export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);
