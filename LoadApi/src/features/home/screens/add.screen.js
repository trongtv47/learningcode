import React, { useState } from 'react';
import { View, Text, TextInput, StyleSheet, TouchableOpacity } from 'react-native';
import { connect } from 'react-redux';
import { addCat } from '../actions';

const AddScreen = ({ navigation, addCat, data }) => {
    const [name, setName] = useState('');
    const [description, setDescription] = useState('');

    function addNewCat() {
        if (!name.length) {
            alert('enter name')
        } else {
            navigation.goBack();
            const newCat = {
                id: data.length + 1,
                name: name,
                description: description,
                image: "https://loremflickr.com/250/250",
                price: "1778.23",
                discount_amount: "669.95",
                counterFavorite: 0,
                status: true,
                categories: [],
            }
            addCat(newCat);
        }
    }

    return (
        <View style={styles.container}>
            <TextInput
                value={name}
                onChangeText={setName}
                placeholder='Nhap ten meo'
            />

            <TextInput
                value={description}
                onChangeText={setDescription}
                placeholder='Nhap mo ta'
            />
            <TouchableOpacity
                onPress={() => {
                    addNewCat()
                }}
            >
                <Text>ADD</Text>
            </TouchableOpacity>
        </View>
    );
};

const mapStateToProps = (state) => {
    return {
        data: state.homeState.data,
    };
};

const mapDispatchToProps = {
    addCat,
};

export default connect(mapStateToProps, mapDispatchToProps)(AddScreen);

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
    }
});