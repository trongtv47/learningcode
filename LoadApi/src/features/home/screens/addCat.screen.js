import React, {useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StyleSheet,
  TextInput,
  Button,
  Alert,
} from 'react-native';

import {connect} from 'react-redux';
import {addCat} from '../actions';

import {Formik, yupToFormErrors} from 'formik';
import * as yup from 'yup';

const AddScreen = ({navigation, addCat, data}) => {
  // const [nameCat, setNameCat] = useState('');
  // const [description, setDescription] = useState('');

  const inputStyle = {
    borderWidth: 1,
    borderColor: '#4e4e4e',
    padding: 12,
    marginBottom: 5,
  };

  let name = '';
  let des = '';

  function addNewCat() {}

  return (
    <Formik
      initialValues={{
        name,
        des,
      }}
      onSubmit={(values) => console.log(values.name)}
      validationSchema={yup.object().shape({
        name: yup
          .string()
          .min(5, 'Name Cat must have min 5 characters')
          .max(30, 'Name Cat have max 30 characters')
          .required('Name Cat is required'),
        des: yup.string().max(20, 'Description Cat have max 100 characters'),
      })}>
      {({
        values,
        handleChange,
        errors,
        setFieldTouched,
        touched,
        isValid,
        handleSubmit,
      }) => (
        <View style={styles.formContainer}>
          <TextInput
            style={inputStyle}
            value={values['name']}
            onChangeText={handleChange('name')}
            onBlur={() => setFieldTouched('name')}
            placeholder="Name Cat"
          />
          {touched.name && errors.name && (
            <Text style={{fontSize: 12, color: 'red'}}>{errors.name}</Text>
          )}
          <TextInput
            value={values['des']}
            style={inputStyle}
            onChangeText={handleChange('des')}
            onBlur={() => setFieldTouched('des')}
            placeholder="Description"
          />
          {touched.des && errors.des && (
            <Text style={{fontSize: 12, color: 'red'}}>{errors.des}</Text>
          )}

          <Button
            color="#3740FE"
            title="Submit"
            disabled={!isValid}
            onPress={handleSubmit}
            onPress={() => {
              navigation.goBack();

              const newCat = {
                id: data.length + 1,
                name: values.name,
                description: values.des,
                image: 'https://loremflickr.com/250/250',
                price: '1778.23',
                discount_amount: '669.95',
                counterFavorite: 0,
                status: true,
                categories: [],
              };
              addCat(newCat);
            }}
          />
          <Button title="Cancel" onPress={() => navigation.goBack()} />
        </View>
      )}
    </Formik>
  );
};

const mapStateToProps = (state) => {
  return {
    data: state.homeState.data,
  };
};

const mapDispatchToProps = {
  addCat,
};

export default connect(mapStateToProps, mapDispatchToProps)(AddScreen);

const styles = StyleSheet.create({
  formContainer: {
    padding: 50,
  },

  container: {
    flex: 1,
    justifyContent: 'center',
    marginHorizontal: 16,
  },

  navBack: {
    fontSize: 30,
    color: 'blue',
  },

  textInnput: {
    height: 40,
    borderWidth: 1,
    margin: 9,
    padding: 9,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },

  fixToText: {
    // flex: 1,
    flexDirection: 'column',
    alignItems: 'stretch',
    padding: 9,
  },

  marginBottom: {
    marginBottom: 9,
  },
});
