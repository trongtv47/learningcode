import * as types from './actionTypes';

const HomeState = {
  data: [],
};

export default function HomeReducer(state = HomeState, action) {

  const { type, data } = action;
  console.log('type: ', type);
  console.log('payload: ', data);
  switch (action.type) {
    case types.GET_ITEMS:
    case types.COUNT_STATE:
      return {
        ...state,
        data: action.data,
      };

    case types.ADD_CAT:
      const newListAdd = [...state.data];
      newListAdd.unshift(action.data);
      return {
        ...state,
        data: newListAdd,
      };
    case types.DEL_CAT:
      console.log('========', action.data);
      const delCat = state.data.filter((item) => item.id !== action.data);
      return {
        ...state,
        data: delCat,
      };
    default:
      return state;
  }
}
