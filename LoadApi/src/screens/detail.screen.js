import React, { useState, useEffect } from 'react';
import {
    View,
    Text,
    ImageBackground,
    StyleSheet,
    TouchableOpacity,
    ScrollView,
    Share,
    Dimensions,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import IconFontAwesome from 'react-native-vector-icons/FontAwesome';
import IconFontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Carousel, { ParallaxImage } from 'react-native-snap-carousel';

const images = [
    {
        img: 'https://i.imgur.com/UPrs1EWl.jpg',
    },

    {
        img: 'https://i.imgur.com/MABUbpDl.jpg',
    },

    {
        img: 'https://i.imgur.com/KZsmUi2l.jpg',
    },

    {
        img: 'https://i.imgur.com/2nCt3Sbl.jpg',
    },
];

const { width: screenWidth } = Dimensions.get('window');

export default function DetailScreen({ navigation, route }) {
    const { data, data2 } = route.params;
    // console.log(`${data.image}?random=${data2}`);

    const [entries, setEntries] = useState([]);

    useEffect(() => {
        setEntries(images);
    }, []);

    const renderItem = ({ item, index }, parallaxProps) => {
        return (
            <View style={styles.item}>
                <ParallaxImage
                    source={{ uri: item.img }}
                    containerStyle={styles.imageContainer}
                    style={styles.image}
                    parallaxFactor={0.4}
                    {...parallaxProps}
                />
            </View>
        );
    };

    const onShare = async () => {
        try {
            const result = await Share.share({
                title: 'App link',
                message: '',
                url:
                    'https://play.google.com/store/apps/details?id=nic.goi.aarogyasetu&hl=en',
            });
            if (result.action === Share.sharedAction) {
                if (result.activityType) {
                    // shared with activity type of result.activityType
                } else {
                    // shared
                }
            } else if (result.action === Share.dismissedAction) {
                // dismissed
            }
        } catch (error) {
            alert(error.message);
        }
    };

    return (
        <ScrollView>
            <View>
                <View
                    style={styles.main}>
                    <TouchableOpacity onPress={() => navigation.goBack()}>
                        <Icon
                            name="chevron-back"
                            style={styles.navBack}></Icon>
                    </TouchableOpacity>

                    <View style={{ flexDirection: 'row' }}>
                        <IconFontAwesome
                            name="star-o"
                            style={styles.navFav}></IconFontAwesome>
                        <Text style={styles.textFavorite}>
                            {data.counterFavorite}
                        </Text>
                        <TouchableOpacity onPress={onShare}>
                            <IconFontAwesome
                                name="share-alt"
                                style={styles.iconButton}></IconFontAwesome>
                        </TouchableOpacity>
                    </View>
                </View>

                <Carousel
                    sliderWidth={screenWidth}
                    sliderHeight={screenWidth}
                    itemWidth={screenWidth - 60}
                    data={entries}
                    renderItem={renderItem}
                    hasParallaxImages={true}
                />
                <View style={styles.flexRow}>
                    <Text style={styles.textName}>{data.name}</Text>
                </View>

                <View style={styles.flexRow}>
                    <Text style={styles.textDescription}>{data.description}</Text>
                </View>
            </View>
        </ScrollView>
    );
}

const styles = StyleSheet.create({

    main: {
        marginTop: 16,
        marginLeft: 9,
        alignContent: 'space-between',
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    scroll: {
        flex: 1,
        justifyContent: 'center',
    },

    container: {
        flex: 1,
    },

    image: {
        ...StyleSheet.absoluteFillObject,
        resizeMode: 'cover',
    },

    imageContainer: {
        flex: 1,
        marginBottom: Platform.select({ ios: 0, android: 1 }), // Prevent a random Android rendering issue
        backgroundColor: 'whiter',
        borderRadius: 8,
    },

    item: {
        width: screenWidth - 60,
        height: screenWidth - 60,
    },
    imageContainer: {
        flex: 1,
        marginBottom: Platform.select({ ios: 0, android: 1 }), // Prevent a random Android rendering issue
        backgroundColor: 'white',
        borderRadius: 8,
    },

    iconButton: {
        marginRight: 9,
        fontSize: 25,
        color: '#558FF6',
    },

    flexRow: {
        flexDirection: 'row',
        fontWeight: 'bold',
        fontSize: 20,
        color: 'white',
        marginLeft: 9,
        marginRight: 9,
    },

    textDescription: {
        flexDirection: 'row',
        fontSize: 14,
        color: '#4c4c4c',
        marginLeft: 9,
        paddingTop: 16,
    },

    textFavorite: {
        fontSize: 20, 
        color: '#558FF6', 
        marginRight: 32
    },


    textName: {
        flexDirection: 'row',
        fontWeight: 'bold',
        fontSize: 16,
        color: '#315fdb',
        marginLeft: 9,
    },

    navBack: {
        fontSize: 30, 
        color: 'blue' 
    },

    navFav: {
        fontSize: 25,
        color: '#558FF6',
        marginRight: 9,
    },
});
