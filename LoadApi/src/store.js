import {createStore, applyMiddleware, combineReducers} from 'redux';
import thunk from 'redux-thunk';
import HomeReducer from './features/home/reducers';

const configureStore = () => {
  const appReducers = combineReducers({
    homeState: HomeReducer,
  });
  let store = createStore(appReducers, applyMiddleware(thunk));
  return store;
};

export default configureStore;
